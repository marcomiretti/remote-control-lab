/*
 * Copyright 2021 ACES.
 *third party local libsin the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */ 
/** \file adc_reader.c */
/* standard */
/* third party libs */
#include <FreeRTOS.h>
#include <task.h>
#include <espressif/esp_common.h>
#include <espressif/user_interface.h>
#include <esp/uart.h>

/* third party local libs */
#include <log.h>

/* configuration includes */
#include <pinout_configuration.h>

/* parameter assignment for ADC */
#define SYSTEM_VOLTAGE                      3.3     /**< \brief Default system voltage */
#define ADC_RESOLUTION                      1023    /**< \brief ADC resolution excluding 0 */

/* global variable: 16-bit integer */
static uint16_t adc_value;

/* global variable: between 0 and 1 */
float adc_voltage;

/**
 * \brief   adc_read.
 */
void adc_read(void *pvParameters) {
    for (;;) {
        adc_value = sdk_system_adc_read();
        adc_voltage = adc_value*(SYSTEM_VOLTAGE/ADC_RESOLUTION);
        log_debug("ADC Voltage: %f", adc_voltage);
        vTaskDelay(10 / portTICK_PERIOD_MS);
    }
}
