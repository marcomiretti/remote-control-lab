/*
 * Copyright 2021 ACES.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */
/** \file potentiometer_dwarf.c */
/* standard */
#include <string.h>

/* third party libs */
#include <FreeRTOS.h>
#include <task.h>
#include <espressif/esp_common.h>
#include <espressif/user_interface.h>
#include <esp/uart.h>

/* third party local libs */
#include <log.h>

/* configuration includes */
#include <pinout_configuration.h>

/* parameter assignment for RESISTANCE */
#define SIZE_RES_VALUES                    9       /**< \brief Array size of resistor values */

/* global variable: integer */
int res_values[SIZE_RES_VALUES] = {220, 50, 10000, 3300, 470, 2200, 470, 3900, 68000};
int resistance;

/**
 * \brief   potentiometer_dwarf.
 */
void potentiometer_dwarf(void *pvParameters) {
    for (;;) {
        for (int i = 0; i < SIZE_RES_VALUES; i ++) {
        resistance = res_values[i];
        log_debug("Resistance : %d", resistance);
        vTaskDelay(5000 / portTICK_PERIOD_MS);
        }
    }
}
